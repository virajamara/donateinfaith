---
title: "Helping people is my happiness."
date: 2019-12-23T20:56:42+06:00
type: portfolio
image: "images/projects/news-paper-2.jpg"
category: ["ARTICLES"]
project_images: ["images/projects/news-paper-2.jpg", "images/projects/logo.jpg"]
---

### ~ Roshan Fernando from Sweden who helps the needy in Sri Lanka ~


**F**or 18 years, he has been helping needy people with all his heart. He used to live in a house thatched with coconut fronds, but today he is a rich man who lives in a big mansion. In his childhood, the same school uniform was washed and worn all five days. Due to his efforts, determination, and dedication, he is now at the top in terms of freedom, happiness, wealth, and power. He has a humble character and gives alms to the needy and poor without expecting any benevolence, not for political gain either. Born in Kochchikade, Negombo, he is **Roshan Fernando**, currently living in Stockholm, Sweden. Having seen his silent social activities on the internet, we contacted him through the social network. This is a conversation with him.

### We Want to know some information about yourself ? 

There are six children in our family: three sons and three daughters. Born in Kochchikade, Negombo, we do not come from a rich family. In 1972, our sister came to Sweden, and then our lives became a strange world. I have two daughters; Melissa and Emma, live with me in Sweden.

### How do you achieve this kind of success in life? 

I come from a very poor family. I remember washing the same white clothes and wearing them to school all five days. Those were the necessities. We had such a harsh life. A person in a good position in today's society hesitates to talk about the past. But I am not such a person.

As of now, four of my siblings are in Sweden, while the other two are living in France. All three of my brothers earn money to help needy people as much as possible.

After our sister, Airani, went to Sweden in 1972, many changes happened in our lives. We started a glass factory in Sri Lanka, even importing nails for it from Sweden. During that time, we were running out of money. I had 48 employees. I was about 18 years old then. Even at that young age, I helped many innocent and helpless people. However, during the July riots in 1983, our businesses collapsed. Later, I went to Sweden and returned to Sri Lanka. In 1999, I moved to Sweden with my children. Yet, I did not cease to perform good deeds for the needy people.

### What social care do you do for the children of Sri Lanka? How can this be done from Sweden?

So far, we have assisted many needy individuals, including those in building houses, visually impaired children, schoolchildren, needy war heroes, disabled war heroes, debate and ballet artists, needy artists, and children with special needs. My intention is not to seek fame through these acts of kindness, but rather to ensure that those who require assistance are aware of the support available to them.

In particular, I have provided significant aid to Gamini's nobles, including 'Veera Mata.' Despite any current wealth, I have not forgotten the struggles of my past, symbolized by the 'battle of the palm leaves.' This sense of fulfillment enriches my life.

There is mutual understanding and appreciation for our efforts, as evidenced by the awards received. Despite residing outside of Sri Lanka, I make an annual visit to the country and continue to support people through the social care projects established by the **Roshan Foundation**.

### Do you have an idea to enter politics? 

I harbor no racism towards Sinhala, Tamil, or Muslim communities. As a Christian, I hold no religious biases either. While many may use such endeavors as a gateway into politics, my motives are entirely distinct, and I have no intention of entering politics. I acknowledge that the entirety of Sri Lanka cannot rely on me for assistance, but I strive to contribute within my means. The satisfaction derived from extending this aid continually rejuvenates my life.

### From the family in doing this kind of social good No objections?

In fact, my two children also support my activities. I work here for 16 hours and allocate 8 hours of money to help the needy in Sri Lanka. My daughter says that she too will grow up and help poor children living in Africa.

Regarding the services offered to artists, the art of Vada Baila is currently in decline, with few individuals stepping forward to preserve it. However, I have been dedicated to conserving the art of Vada Baila for many years. I have organized numerous concerts in the Negombo area for Vada Baila and Baila artists, paying tribute to them and providing financial support. Additionally, I have worked to elevate our Vada Baila art to the international level.

While I do not possess any special talent in music, I have extended support to senior singers, including Pandit Amaradevayan and Wisarada Gunadasa Kapuge, by continuing to pay their house electricity bills when they were in need. I have also sung some ballads and composed the first Ranaviru song after saving our country from LTTE terrorists.

Furthermore, arrangements have been made for my solo concert in Sweden on October 28th, where I hope to pay tribute to artists like Wally Bastian, M. S. Fernando, Nihal Nelson, and the Gypsies' Sunil through their songs.

### Why do you help the needy people like this?

No one takes anything from this world when they die. A person should have merit to give and receive something. I engage in these activities joyfully. I allocate my earnings towards helping those in need. When I contemplate the world, I realize I lack nothing as a person like myself.

My latest endeavor involves initiating "Ran Kekulu" Savings Accounts for 100 children, each receiving Rs 1000 per month. This initiative costs 6 1/2 lakhs per year, and by its conclusion, crores will be expended. Additionally, I have implemented a program to distribute 100 water tanks to impoverished villages across Sri Lanka. Furthermore, I aim to provide employment opportunities for unemployed Sri Lankans, not only in Sri Lanka but also in Sweden, Paris, and beyond.

For those interested in contacting me, you can reach me via email at r.karna@hotmail.com or through Facebook.
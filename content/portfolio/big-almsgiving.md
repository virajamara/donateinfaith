---
title: "Rice Dansala and Books Donation Programme"
date: 2019-12-23T20:56:42+06:00
type: portfolio
image: "images/projects/dansala/1.png"
category: ["ARTICLES"]
---

### Documentary Videos

#### Video 1 
{{< youtube 7ySyNgjJfdQ >}}

#### Video 2 - Books Donation
{{< youtube 1wdCuqbBgJM >}}

#### Video 3
{{< youtube fQNbbRWdkyE >}}


---
title: "Indigenous children celebrate Children's Day."
date: 2019-12-23T20:56:42+06:00
type: portfolio
image: "images/projects/adiwasi/5.jpg"
category: ["ARTICLES"]
project_images: ["images/projects/adiwasi/2.jpg","images/projects/adiwasi/3.jpg","images/projects/adiwasi/4.jpg","images/projects/adiwasi/6.jpg","images/projects/adiwasi/8.jpg"]
project_videos: ["https://www.youtube.com/embed/Lx75CusKb60?si=oEVRXnDjwOkfvIA3","https://www.youtube.com/embed/XXSiXkB3DI8?si=FRh3awyNa-gw1zPB"]
---

### First Time In the History of Sri Lanka

{{< youtube XXSiXkB3DI8 >}}

On World Children’s Day, children are the focus of everyone’s attention across the country. However, there are few who specifically address the needs of indigenous children. This year, a World Children’s Day celebration was held at Garukumbura Kanishta Vidyalaya in Dambana Garu Kumbura village with the support of Adi Village. The event was organized by the Roshan Fernando Foundation.

The sons of tribal leader Vishva Keerthi Vanaspathi Vannila Bhavan participated in this event, and he remarked that such attention and care for tribal children are truly admirable.

{{< youtube Lx75CusKb60 >}}
{{< youtube UGCwDbZMiqA >}}


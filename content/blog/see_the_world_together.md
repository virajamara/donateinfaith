---
title: "See The World Together"
date: 2024-04-14T13:45:06+06:00
image: images/blog/blog.png
feature_image: blog.png
author: Roshan Fernando
---

#### Helping Hands - Help for Differently Abled in Nayanaloka Village, Negombo

### Piyadasa's Story

{{< youtube G-X4FBZUWhU >}}

### Neelawathi's Story

{{< youtube 2yAgIGooLH0 >}}

### Sugathapla's Story

{{< youtube 9gZ6qdd_fQM >}}

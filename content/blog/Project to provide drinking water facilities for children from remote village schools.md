---
title: "Project to provide drinking water facilities for children from remote village schools"
date: 2024-04-14T13:45:06+06:00
image: images/blog/blog.png
feature_image: blog.png
author: Roshan Fernando
---

#### Project to provide drinking water facilities for children from remote village schools.

### Video

{{< youtube 3dWKI3Hc44g >}}
